local meta = FindMetaTable( "PhysObj" )
if (meta) then
	// In this file we're adding functions to the physics object meta table.
	// This means you'll be able to call functions here straight from the physics object
	// You can even override already existing functions.

	if ( !meta.SetAngleVelocity ) then

		function meta:SetAngleVelocity( velocity )
			if (velocity == nil) then
				velocity = VectorRand()
			end

			self:AddAngleVelocity( -self:GetAngleVelocity() + velocity )

		end

	end


	local meta = FindMetaTable( "Player" )
	if (!meta) then return end

	local Give = meta.Give

	local lua_weapons	= CreateConVar( "lua_weapons",	0, { FCVAR_ARCHIVE, FCVAR_NOTIFY, FCVAR_REPLICATED } )

	function meta:Give( item )

		if ( lua_weapons:GetBool() ) then

			if ( table.HasValue( HL2_WEAPONS, item:lower() ) ) then

				return Give( self, string.Replace( item, "weapon_", "swep_" ) )

			end

		end

		return Give( self, item )

	end

	local function PlayerCanPickupWeapon( player, entity )

		if ( !lua_weapons:GetBool() ) then return end

		if ( table.HasValue( HL2_WEAPONS, entity:GetClass():lower() ) ) then

			local Data	= {}
			Data.Model	= entity:GetModel()
			Data.Pos	= entity:GetPos()
			Data.Angle	= entity:GetAngles()

			entity:Remove()

			local wep = ents.Create( string.Replace( entity:GetClass(), "weapon_", "swep_" ) )
			duplicator.DoGeneric( wep, Data )
			wep:Spawn()

			return false

		end

	end

	hook.Add( "PlayerCanPickupWeapon", "PlayerCanPickupWeapon", PlayerCanPickupWeapon )
end

local ents = {

	"crossbow_bolt",
	"npc_grenade_frag",
	"rpg_missile"

}

/*
this function is broken
it might be the fix for some weapons not doing damage
local function EntityTakeWeaponDamage( ent, inflictor, attacker, amount, dmginfo )

	if (!inflictor:IsValid()) then return end
	
	local pClass 	= inflictor:GetClass()
	local pOwner 	= inflictor:GetOwner()

	if (table.HasValue( ents, pClass )) then

		if (inflictor.m_iDamage) then

			dmginfo:SetDamage( inflictor.m_iDamage )

		end

		if (pOwner && pOwner:IsValid()) then

			dmginfo:SetAttacker( pOwner )

		end

	end

end

hook.Add( "EntityTakeDamage", "EntityTakeWeaponDamage", EntityTakeWeaponDamage )
*/
