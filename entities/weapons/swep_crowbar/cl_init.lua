
include('shared.lua')


SWEP.PrintName			= "A FUCKING CROWBAR!"		// 'Nice' Weapon name (Shown on HUD)
SWEP.ClassName			= "swep_crowbar"
SWEP.Slot				= 0						// Slot in the weapon selection menu
SWEP.SlotPos			= 1						// Position in the slot
SWEP.DrawAmmo			= false					// Should draw the default HL2 ammo counter
SWEP.DrawCrosshair		= true 					// Should draw the default crosshair
SWEP.DrawWeaponInfoBox	= false					// Should draw the weapon info box
SWEP.BounceWeaponIcon   = false					// Should the weapon icon bounce?

// Override this in your SWEP to set the icon in the weapon selection
SWEP.WepSelectFont		= "CreditsLogo"
SWEP.WepSelectLetter	= "c"
SWEP.IconFont			= "HL2MPTypeDeath"
SWEP.IconLetter			= "6"

killicon.AddFont( SWEP.ClassName, SWEP.IconFont, SWEP.IconLetter, Color( 255, 80, 0, 255 ) )

function SWEP:DrawWeaponSelection( x, y, wide, tall, alpha )

	// Set us up the texture
	surface.SetDrawColor( color_transparent )
	surface.SetTextColor( 255, 220, 0, alpha )
	surface.SetFont( self.WepSelectFont )
	local w, h = surface.GetTextSize( self.WepSelectFont )

	// Draw that mother
	surface.SetTextPos( x + ( wide / 2 ) - 65,
						y + ( tall / 2 ) - 77)
	surface.DrawText( self.WepSelectLetter )

end

