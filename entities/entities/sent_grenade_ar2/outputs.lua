local explosionSoundTable = {"weapons/mudkip_boom1.wav", "weapons/mudkip_boom2.wav", "weapons/mudkip_boom3.wav", "weapons/mudkip_boom4.wav", "weapons/mudkip_boom5.wav"}

sound.Add(
{
    name = "Mudkip_Weapon_AR2_Explode",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = explosionSoundTable
})

ENT.Sound				= {}
ENT.Sound.Explode		= "Mudkip_Weapon_AR2_Explode"

function ENT:DoExplodeEffect()

	local info = EffectData();
	info:SetEntity( self.Entity );
	info:SetOrigin( self.Entity:GetPos() );

	util.Effect("HelicopterMegaBomb", info)
	self.Entity:EmitSound( "Mudkip_Weapon_AR2_Explode" );

end

function ENT:OnExplode( pTrace )
	if ((pTrace.Entity != game.GetWorld()) || (pTrace.HitBox != 0)) then
		// non-world needs smaller decals
		if( pTrace.Entity && !pTrace.Entity:IsNPC() ) then
			util.Decal( "SmallScorch", pTrace.HitPos + pTrace.HitNormal, pTrace.HitPos - pTrace.HitNormal );
		end
	else
		util.Decal( "Scorch", pTrace.HitPos + pTrace.HitNormal, pTrace.HitPos - pTrace.HitNormal );
	end
end

/*---------------------------------------------------------
   Name: OnInitialize
---------------------------------------------------------*/
function ENT:OnInitialize()
end

/*---------------------------------------------------------
   Name: StartTouch
---------------------------------------------------------*/
function ENT:StartTouch( entity )
end

/*---------------------------------------------------------
   Name: EndTouch
---------------------------------------------------------*/
function ENT:EndTouch( entity )
end

function ENT:Touch( pOther )

	assert( pOther );
	if ( pOther:GetSolid() == SOLID_NONE ) then
		return;
	end

	// If I'm live go ahead and blow up
	if (self.m_bIsLive) then
		self:Detonate();
	else
		// If I'm not live, only blow up if I'm hitting an chacter that
		// is not the owner of the weapon
		local pBCC = pOther;
		if (pBCC && self.Entity:GetOwner() != pBCC) then
			self.m_bIsLive = true;
			self:Detonate();
		end
	end

end

/*---------------------------------------------------------
   Name: OnThink
---------------------------------------------------------*/
function ENT:OnThink()
end