include("shared.lua")

function ENT:Initialize()
	self.Entity.ParticleTime = 0
end

function ENT:Draw()
	self.Entity:DrawModel()
end

function ENT:Think()
	if self.Entity:GetDTInt(0) == 1 then
		if CurTime() > self.Entity.ParticleTime then
			local forward = self.Entity:GetForward()
			//ParticleEffect("rpg7_smoke_full", self.Entity:GetPos() - forward * 7, self.Entity:GetAngles(), nil)
			//ParticleEffect("rpg7_muzzle_full", self.Entity:GetPos() - forward * 5, self.Entity:GetAngles(), nil)
			self.Entity.ParticleTime = CurTime() + 0.015
		end
		
		local dlight = DynamicLight(self:EntIndex())
		
		if dlight then
			dlight.Pos = self:GetPos()
			dlight.r = 255
			dlight.g = 255
			dlight.b = 100
			dlight.Brightness = 2
			dlight.Size = 384
			dlight.Decay = 384 * 3
			dlight.DieTime = CurTime() + 0.1
		end
	end
end 