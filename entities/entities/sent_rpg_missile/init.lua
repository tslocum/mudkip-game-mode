AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

local explosionSoundTable = {"weapons/mudkip_boom1.wav", "weapons/mudkip_boom2.wav", "weapons/mudkip_boom3.wav", "weapons/mudkip_boom4.wav", "weapons/mudkip_boom5.wav"}

sound.Add(
{
    name = "Mudkip_Weapon_RPG_Explode",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 90,
    sound = explosionSoundTable
})

function ValidEntity( object )
        if (!object) then return false end
        return object:IsValid()
end

function VerifyAngle( a )
    while a > 360 do a = a - 360 end
    while a < 0 do a = a + 360 end
    return a
end
 
function VectorToAngle( self )
    local p, y = 0, 0
     
    -- Pitch by angle between with acos( a dot b )
    local norm = self:Normalize()
    local norm2 = Vector( self.x, self.y, 0 ):Normalize()
    p = math.deg( math.acos( norm:Dot(norm2) ) )
     
    -- Yaw with atan( y / x )
    if self.x == 0 then
        if self.y > 0 then
            y = 90
        elseif self.y < 0 then
            y = 180
        end
    else
        y = math.deg( math.atan( self.y / self.x ) )
    end
     
    p = VerifyAngle( -p )
    y = VerifyAngle( y )
     
    return Angle( p, y, 0 )
end
 
function AngleToVector( self )
    local x = math.cos( math.rad(self.y) )
    local y = math.sin( math.rad(self.y) )
    local z = -math.sin( math.rad(self.p) )
    return Vector( x, y, z )
end

function ENT:Initialize()
	self.Entity:SetModel("models/weapons/w_missile_launch.mdl") 
	self.Entity:PhysicsInit(SOLID_VPHYSICS)
	self.Entity:SetMoveType(MOVETYPE_FLYGRAVITY)
	self.Entity:SetMoveCollide( MOVECOLLIDE_FLY_BOUNCE ) // from older script
	self.Entity:SetSolid(SOLID_VPHYSICS)
	self.Entity:SetCollisionGroup(COLLISION_GROUP_NPC)
	self.Entity:SetGravity(0.1) // was 0.3
	self.Entity:SetDTInt(0, 1)
	self.Entity.RocketSpeed = 450
	local phys = self.Entity:GetPhysicsObject()

	if phys and phys:IsValid() then
		phys:Wake()
	end
	
	self.Entity.RocketSound = CreateSound(self.Entity, "weapons/rpg/mudkip_rocket1_alt.wav")
	self.Entity.RocketSound:SetSoundLevel(80)
	self.Entity.RocketSound:Play()
	
	self:CreateSmokeTrail();
	
	// Allow reloading after three seconds
	// I want to try multiple rockets but it still doesn't let you reload
	timer.Simple(3, function()
		if ValidEntity(self.Entity) then
			local owner = self.Entity:GetOwner()
			
			if ValidEntity(owner) and owner:Alive() then
				owner.CanReload = true
			end
		end
	end)
	
	/*timer.Simple(6, function()
		if ValidEntity(self.Entity) then
			self.Entity:SetDTInt(0, 0) -- oh fuck, we're out of fuel!
			local vel = self.Entity:GetVelocity()
			
			self.Entity:PhysicsInit(SOLID_VPHYSICS)
			self.Entity:SetMoveType(MOVETYPE_VPHYSICS)
			self.Entity:SetSolid(SOLID_VPHYSICS)
			self.Entity:SetCollisionGroup(COLLISION_GROUP_NPC)
			local phys = self.Entity:GetPhysicsObject()

			if phys and phys:IsValid() then
				phys:Wake()
				phys:SetMass(5)
				phys:SetVelocity(vel)
			end
			
			self.Entity.RocketSound:FadeOut(0.3)
			
			local owner = self.Entity:GetOwner()
			
			if ValidEntity(owner) and owner:Alive() then
				owner.CanReload = true
			end
		end
	end)*/
end

function ENT:OnTakeDamage(dmginfo)
	self.bIsDamaged = true
	return false
end

function ENT:Use(activator, caller)
	return false
end

function ENT:Think()
	if ValidEntity(self.Entity.TargetedEntity) then
		if not self.Entity.Velocity then
			self.Entity.Velocity = self.Entity:GetVelocity()
		else
			self.Entity.Velocity = LerpVector(0.4, self.Entity.Velocity, (self.Entity.TargetedEntity:GetPos() - pos):Normalize() * self.Entity.RocketSpeed)
		end
		
		self.Entity:SetLocalVelocity(self.Entity.Velocity)
	end
	
	/*
	Velocity test.  This code basically throws the missile at the ground and it blows up.
	
	local vecForward;

	// !!!UNDONE - make this work exactly the same as HL1 RPG, lest we have looping sound bugs again!
	//self.Entity:EmitSound( "Missile.Accelerate" );
	//self.Entity:EmitSound(missileSound);

	//self:AddEffects( EF_LIGHT );

	vecForward = AngleToVector( self.Entity:GetLocalAngles() );
	self.Entity:SetVelocity( vecForward * RPG_SPEED );
	
	self.Entity:SetAngles(self.Entity:GetVelocity():Angle())

	//self.Think = self.SeekThink;
	//self.Entity:NextThink( CurTime() + 0.1 );
	*/
	
	// I can't get the other types to work yet
	self.Entity.MissileType = 2
	
	if SERVER then
		if self.Entity:GetDTInt(0) == 1  then
			local owner = self.Entity:GetOwner()
			local pos = self.Entity:GetPos()
			
			if self.Entity.MissileType == 1 then
				//self.Entity.RocketSpeed = 1750
				
				if not self.Entity.TargetedEntity then
					for k, v in pairs(ents.FindInSphere(pos, 1024)) do
						if (v:IsPlayer() and v:Alive()) or v:IsNPC() then
							local DotProduct = self.Entity:GetForward():DotProduct((v:GetPos() - pos):GetNormal())
							// old: local DotProduct = self.Entity:GetForward():DotProduct((v:GetPos() - pos):Normalize())
							
							if true or (DotProduct >= 0.7 and v != owner) then
								local traceDir = (v:GetPos() - pos):GetNormal()
								// old: local traceDir = (v:GetPos() - pos):Normalize()
								
								local td = {}
								
								td.start = pos
								td.endpos = td.start + traceDir * 1024 + Vector(0, 0, 30) -- make sure we're not running the trace at the entity's feet
								td.filter = {}
								
								for k2, v2 in pairs(ents.FindByClass("trigger_soundscape")) do
									table.insert(td.filter, v2)
								end
								
								table.insert(td.filter, self.Entity)
								
								local trace = util.TraceLine(td)
								
								if trace.Entity == v then
									self.Entity.TargetedEntity = v
									break
								end
							end
						end
					end
				end
				
				if not self.Entity.TargetedEntity or self.Entity.TargetedEntity:GetClass() != "ent_missile" then
					for k, v in pairs(ents.FindInSphere(pos, 1024)) do
						if v:GetClass() == "ent_missile" and v != self.Entity then
							local DotProduct = self.Entity:GetForward():DotProduct((v:GetPos() - pos):GetNormal())
							// old: local DotProduct = self.Entity:GetForward():DotProduct((v:GetPos() - pos):Normalize())
							
							if DotProduct >= 0.6 then
								local traceDir = (v:GetPos() - pos):Normalize()
								
								local td = {}
								
								td.start = pos
								td.endpos = td.start + traceDir * 1024 + Vector(0, 0, 30) -- make sure we're not running the trace at the entity's feet
								td.filter = {}
								
								for k2, v2 in pairs(ents.FindByClass("trigger_soundscape")) do
									table.insert(td.filter, v2)
								end
								
								table.insert(td.filter, self.Entity)
								
								local trace = util.TraceLine(td)
								
								if trace.Entity == v then
									self.Entity.TargetedEntity = v
									self.Entity.RocketSpeed = 2000
									break
								end
							end
						end
					end
				end
				
				if ValidEntity(self.Entity.TargetedEntity) then
					if not self.Entity.Velocity then
						self.Entity.Velocity = self.Entity:GetVelocity()
					else
						self.Entity.Velocity = LerpVector(0.4, self.Entity.Velocity, (self.Entity.TargetedEntity:GetPos() - pos):GetNormal() * self.Entity.RocketSpeed)
						// old: self.Entity.Velocity = LerpVector(0.4, self.Entity.Velocity, (self.Entity.TargetedEntity:GetPos() - pos):Normalize() * self.Entity.RocketSpeed)
					end
					
					self.Entity:SetLocalVelocity(self.Entity.Velocity)
				end
				
				if self.Entity.TargetedEntity and not ValidEntity(self.Entity.TargetedEntity) then
					self.Entity.TargetedEntity = nil
					self.Entity.MissileType = 0
				end
					
			elseif self.Entity.MissileType == 2 and owner:Alive() then
				//self.Entity.RocketSpeed = 1500
				local ownerVel = owner:GetVelocity():Length()
				local aimVec = owner:GetAimVector()
				
				local td = {}
				td.start = owner:GetShootPos()
				td.endpos = td.start + aimVec * 8192
				td.filter = {}
								
				for k2, v2 in pairs(ents.FindByClass("trigger_soundscape")) do
					table.insert(td.filter, v2)
				end
								
				table.insert(td.filter, self.Entity)
				table.insert(td.filter, owner)
				
				local trace = util.TraceLine(td)
				
				if not self.Entity.Velocity then
					self.Entity.Velocity = (trace.HitPos - self.Entity:GetPos()):GetNormal() * self.Entity.RocketSpeed
					// old: self.Entity.Velocity = (trace.HitPos - self.Entity:GetPos()):Normalize() * self.Entity.RocketSpeed
				else
					self.Entity.Velocity = LerpVector(0.3, self.Entity.Velocity, (trace.HitPos - self.Entity:GetPos()):GetNormal() * self.Entity.RocketSpeed)
					// old: self.Entity.Velocity = LerpVector(0.4, self.Entity.Velocity, (trace.HitPos - self.Entity:GetPos()):Normalize() * self.Entity.RocketSpeed)
				end

				local randomShake 
				local random = math.random(-1, 1)
				
				if ownerVel > 0 then
					randomShake = Vector(random, random, random) * (ownerVel * 0.35)
				else
					randomShake = Vector(0, 0, 0)
				end

				self.Entity:SetLocalVelocity(self.Entity.Velocity + randomShake)
			elseif self.Entity.MissileType == 3 and self.Entity.MarkedPos and CurTime() > self.Entity.ArmTime then
				//self.Entity.RocketSpeed = 1750
				
				if not self.Entity.Velocity then
					self.Entity.Velocity = self.Entity:GetVelocity() //(self.Entity.MarkedPos - self.Entity:GetPos()):Normalize() * self.Entity.RocketSpeed
				else
					self.Entity.Velocity = LerpVector(0.4, self.Entity.Velocity, (self.Entity.MarkedPos - self.Entity:GetPos()):Normalize() * self.Entity.RocketSpeed)
				end
				
				self.Entity:SetLocalVelocity(self.Entity.Velocity)
			end
			
			self.Entity:SetAngles(self.Entity:GetVelocity():Angle())
		end
	end
	
	
end

function ENT:Touch(ent)
	local owner = self.Entity:GetOwner()
	
	if ent != owner and ent:GetClass() != "trigger_soundscape" then
		self.Entity.MissileType = 0
		self.Entity:Explode()
	end
end

function ENT:PhysicsCollide(data, physobj)
	self.Entity.MissileType = 0
	self.Entity:Explode()
end

function ENT:OnRemove()
	self.Entity.RocketSound:Stop()
end

function ENT:CreateSmokeTrail()

	if ( self.m_hRocketTrail ) then
		return;
	end
	ParticleEffectAttach( "Rocket_Smoke", PATTACH_ABSORIGIN_FOLLOW, self, 0 );

end

function ENT:DoExplosion()

	ExplosionCreate( GetAbsOrigin(), GetAbsAngles(), GetOwnerEntity(), GetDamage(), GetDamage() * 2,
		bit.bor(SF_ENVEXPLOSION_NOSPARKS, SF_ENVEXPLOSION_NODLIGHTS, SF_ENVEXPLOSION_NOSMOKE), 0.0, this);

end

function ENT:OnExplode( pTrace )
	if ((pTrace.Entity != game.GetWorld()) || (pTrace.HitBox != 0)) then
		if( pTrace.Entity && !pTrace.Entity:IsNPC() ) then
			util.Decal( "SmallScorch", pTrace.HitPos + pTrace.HitNormal, pTrace.HitPos - pTrace.HitNormal );
			Msg("Created small scorch\n")
		end
	else
		util.Decal( "Scorch", pTrace.HitPos + pTrace.HitNormal, pTrace.HitPos - pTrace.HitNormal );
		Msg("Created big scorch\n")
	end
end

function ENT:Explode()
	if self.Entity.BlewUp then
		return
	end
	
	local owner = self.Entity:GetOwner()
	
	local ED = EffectData()
	ED:SetOrigin(self.Entity:GetPos())
	
	util.Effect("HelicopterMegaBomb", ED)
	
	self.Entity:EmitSound( "Mudkip_Weapon_RPG_Explode" );
	
	local vecForward = self.Entity:GetVelocity();
	vecForward = vecForward:GetNormal();
	// was vecForward = VectorNormalize(vecForward);
	local		tr;
	tr = {};
	tr.start = self.Entity:GetPos();
	tr.endpos = self.Entity:GetPos() + 60*vecForward;
	tr.mask = MASK_SHOT;
	tr.filter = self;
	tr.collision = COLLISION_GROUP_NONE;
	tr = util.TraceLine ( tr);
	
	self:OnExplode( tr );
	
	if ValidEntity(owner) and owner:Alive() then
		owner.CanReload = true
	end
	
	for k, v in pairs(ents.FindInSphere(self.Entity:GetPos(), 256)) do
		if ValidEntity(v) then
			if not v:IsPlayer() or not v:IsNPC() and (v:GetClass() == "prop_physics" or v:GetClass() == "prop_ragdoll") then
			
				local phys = v:GetPhysicsObject()
				
				if not v.ReceivedExplosiveForce then
					v.ReceivedExplosiveForce = 300
				else
					v.ReceivedExplosiveForce = v.ReceivedExplosiveForce + 300
				end
				
				if ValidEntity(phys) then
					local distance = self.Entity:GetPos():Distance(v:GetPos())
					local relation = math.Clamp(((256 - distance) / 256) / (phys:GetMass() * 0.005), 0, 1)
					local forceDir = ((v:GetPos() - self.Entity:GetPos()):GetNormal() * 1000) * relation
					
					if phys:IsMoveable() then
						phys:AddAngleVelocity(Vector(1000, 1000, 200) * relation)
						phys:SetVelocity(forceDir)
					end
					
					if v.ReceivedExplosiveForce >= phys:GetMass() then
						constraint.RemoveAll(v)
						
						phys:Wake()
						phys:EnableMotion(true)
						
						phys:AddAngleVelocity(Vector(1000, 1000, 200) * relation)
						phys:SetVelocity(forceDir)
						
						v.ReceivedExplosiveForce = 0
					end
				end
				
			end
		end
	end
	
	if( self.m_hRocketTrail ) then
		self.m_hRocketTrail:SetLifetime(0.1);
		self.m_hRocketTrail = NULL;
	end
	
	util.BlastDamage(self.Entity, owner, self.Entity:GetPos(), 512, 120)
	
	SafeRemoveEntity(self.Entity)
end