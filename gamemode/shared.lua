GM.Name 	= "Mudkip"
GM.Author 	= "Rocket Number Nine"
GM.Email 	= "tslocum@gmail.com"
GM.Website 	= "http://notblog.org"

TEAM_TRAINER	= 1
TEAM_MUDKIP	= 2
TEAM_DEATHMATCH	= 3
TEAM_SPECTATOR	= 4

 
DeriveGamemode( "base" )

function GM:IsTeamPlay()
	if (string.sub(string.lower(game.GetMap()), 1, 4) == "tmu_") then
		return true
	else
		return false
	end
end

function GM:GetTeamList()
	if(GAMEMODE:IsTeamPlay()) then
		return {TEAM_TRAINER, TEAM_MUDKIP}
	else
		return {TEAM_DEATHMATCH}
	end
end

function GM:CreateTeams()
	self.TeamBased = GAMEMODE:IsTeamPlay()
	if(self.TeamBased) then
		team.SetUp(TEAM_TRAINER, "Trainers", Color(254, 0, 0, 225))
		team.SetSpawnPoint(TEAM_TRAINER, { "info_player_trainer", "info_player_red", "info_player_deathmatch" } )

		team.SetUp(TEAM_MUDKIP, "Mudkips", Color(102, 153, 204, 225)) 
		team.SetSpawnPoint(TEAM_MUDKIP, { "info_player_mudkip", "info_player_blue", "info_player_deathmatch" } )
	else
		team.SetUp(TEAM_DEATHMATCH, "Players", Color(102, 153, 204, 225)) 
		team.SetSpawnPoint(TEAM_DEATCHMATCH, { "info_player_trainer", "info_player_mudkip", "info_player_red", "info_player_blue", "info_player_deathmatch" } )
	end
	
	team.SetUp(TEAM_SPECTATOR, "Spectators", Color(200, 200, 200), true )
	team.SetSpawnPoint(TEAM_SPECTATOR, "info_player_start" )
	team.SetClass(TEAM_SPECTATOR, { "Spectator" } )
end

function GM:IsPlayerTeam(teamid)
	if(GAMEMODE:IsTeamPlay()) then
		return (teamid == TEAM_TRAINER || teamid == TEAM_MUDKIP)
	else
		return (teamid == TEAM_DEATHMATCH)
	end
end

function GM:EntityTakeDamage( ent, inflictor, attacker, amount, dmginfo )
	if ( ent:IsPlayer() ) then
			//ent:OnTakeDamage( inflictor, attacker, amount, dmginfo );
			
			if( ent.Info and ent.Info.Pain ) then
					ent:RandomQuip( ent.Info.Pain );
			end
	end
	
	if( inflictor == attacker and ( attacker:IsPlayer() or attacker:IsNPC() ) ) then
			inflictor = attacker:GetActiveWeapon()
	end
	
	if( inflictor.WeaponHurtEnt != nil ) then
			inflictor:WeaponHurtEnt( entity, amount, dmginfo )
	end

	if( IsValid(dmginfo) and dmginfo:GetAttacker():IsPlayer() and GAMEMODE:EntityFitsFilter( GAMEMODE.HitIndicatorEnts, ent:GetClass() ) and GAMEMODE:ShouldDrawHitIndicator() == true ) then
			GAMEMODE:SendHitIndicator( dmginfo:GetAttacker(), dmginfo:GetDamage()/100 );
	end
end

function GM:GetFallDamage( ply, speed )
	return (speed / 11)
end

function GM:GetRagdollEyes( ply )
        local Ragdoll = nil
        
        Ragdoll = ply:GetRagdollEntity()
        if ( !Ragdoll ) then return end
        
        local att = Ragdoll:GetAttachment( Ragdoll:LookupAttachment("eyes") )
        if ( att ) then
        
                local RotateAngle = Angle( math.sin( CurTime() * 0.5 ) * 30, math.cos( CurTime() * 0.5 ) * 30, math.sin( CurTime() * 1 ) * 30 ) 

                att.Pos = att.Pos + att.Ang:Forward() * 1
                att.Ang = att.Ang
                
                return att.Pos, att.Ang
        
        end
end

 