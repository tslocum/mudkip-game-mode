include( 'shared.lua' )

function team_menu()
	local TeamFrame
	local TeamTitleLabel
	local ButtonMudkips
	local ButtonTrainers
	
	TeamFrame = vgui.Create('DFrame')
	TeamFrame:SetSize(126, 98)
	TeamFrame:Center()
	TeamFrame:SetTitle('')
	TeamFrame:SetDeleteOnClose(false)
	TeamFrame:SetBackgroundBlur(false)
	
	TeamTitleLabel = vgui.Create('DLabel', TeamFrame)
	TeamTitleLabel:SetPos(4,  26)
	TeamTitleLabel:SetText('CHOOSE YOUR DESTINY')
	TeamTitleLabel:SizeToContents()
	TeamTitleLabel:SetTextColor(Color(255, 255, 255, 255))
	
	ButtonMudkips = vgui.Create('DButton', TeamFrame)
	ButtonMudkips:SetSize(122, 25)
	ButtonMudkips:SetPos(1, 44)
	ButtonMudkips:SetText('Mudkips')
	ButtonMudkips:ColorTo(Color(102, 153, 204, 225 ), 0.25, 0)
	ButtonMudkips.DoClick = function()
		RunConsoleCommand( "team_mudkip" )
		TeamFrame:SetVisible(false)
	end

	ButtonTrainers = vgui.Create('DButton', TeamFrame)
	ButtonTrainers:SetSize(122, 25)
	ButtonTrainers:SetPos(1, 71)
	ButtonTrainers:SetText('Trainers')
	ButtonTrainers:ColorTo(Color(254, 0, 0, 225 ), 0.25, 0)
	ButtonTrainers.DoClick = function()
		RunConsoleCommand( "team_trainer" )
		TeamFrame:SetVisible(false)
	end
	
	TeamFrame:MakePopup()
end

concommand.Add("team_menu", team_menu)

function MudkipShowTeam( ply ) // Key F2
	ply:ConCommand( "team_menu" )
end
hook.Add("ShowTeam", "MudkipShowTeam", MudkipShowTeam)

local function CalcView( pl, origin, angles, fov )
	local ragdoll = pl:GetRagdollEntity();
	if(!ragdoll || ragdoll == NULL || !ragdoll:IsValid()) then return; end
	
	local eyes = ragdoll:GetAttachment( ragdoll:LookupAttachment( "eyes" ) );
	local eyes_pos = eyes.Pos
	local eyes_angles = eyes.Ang
	
	local view = {
		origin = eyes_pos,
		angles = eyes_angles,
		fov = 90, 
	};

	return view;
end
hook.Add( "CalcView", "DeathView", CalcView );
