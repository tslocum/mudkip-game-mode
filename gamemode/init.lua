AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

local AnimTranslateTable = {}
AnimTranslateTable[ PLAYER_RELOAD ] 	= ACT_HL2MP_GESTURE_RELOAD
AnimTranslateTable[ PLAYER_JUMP ] 		= ACT_HL2MP_JUMP
AnimTranslateTable[ PLAYER_ATTACK1 ] 	= ACT_HL2MP_GESTURE_RANGE_ATTACK

local function MudkipPlayerAnimation( pl, anim )

	local act = ACT_HL2MP_IDLE
	local Speed = pl:GetVelocity():Length()
	local OnGround = pl:OnGround()

	// If it's in the translate table then just straight translate it
	if ( AnimTranslateTable[ anim ] != nil ) then

		act = AnimTranslateTable[ anim ]

	else

		// Crawling on the ground
		if ( OnGround && pl:Crouching() ) then

			act = ACT_HL2MP_IDLE_CROUCH

			if ( Speed > 0 ) then
				act = ACT_HL2MP_WALK_CROUCH
			end

		elseif (Speed > 150) then

			act = ACT_HL2MP_RUN

		// Player is running on ground
		elseif (Speed > 0) then

			act = ACT_HL2MP_WALK

		end

	end

	// Attacking/Reloading is handled by the RestartGesture function
	if ( act == ACT_HL2MP_GESTURE_RANGE_ATTACK ||
		 act == ACT_HL2MP_GESTURE_RELOAD ) then

		pl:RestartGesture( pl:Weapon_TranslateActivity( act ) )

		// If this was an attack send the anim to the weapon model
		if (act == ACT_HL2MP_GESTURE_RANGE_ATTACK) then

			pl:Weapon_SetActivity( pl:Weapon_TranslateActivity( ACT_RANGE_ATTACK1 ), 0 );

		end

	return end

	// Always play the jump anim if we're in the air
	if ( !OnGround ) then

		act = ACT_HL2MP_JUMP

	end

	// Ask the weapon to translate the animation and get the sequence
	// ( ACT_HL2MP_JUMP becomes ACT_HL2MP_JUMP_AR2 for example)
	local seq = pl:SelectWeightedSequence( pl:Weapon_TranslateActivity( act ) )

	// If we're in a vehicle just sit down
	// We should let the vehicle decide this when we have scripted vehicles
	if (pl:InVehicle()) then

		// TODO! Different ACTS for different vehicles!
		local pVehicle = pl:GetVehicle()

		if ( pVehicle.HandleAnimation != nil ) then

			seq = pVehicle:HandleAnimation( pl )
			if ( seq == nil ) then return end

		else

			local class = pVehicle:GetClass()

			//
			// To realise why you don't need to add to this list,
			// See how the chair handles this ( HandleAnimation )
			//
			if ( class == "prop_vehicle_jeep" ) then
				seq = pl:LookupSequence( "drive_jeep" )
			elseif ( class == "prop_vehicle_airboat" ) then
				seq = pl:LookupSequence( "drive_airboat" )
			else
				seq = pl:LookupSequence( "drive_pd" )
			end

		end

	end



	// If the weapon didn't return a translated sequence just set
	//	the activity directly.
	if (seq == -1) then

		// Hack.. If we don't have a weapon and we're jumping we
		// use the SLAM animation (prevents the reference anim from showing)
		if (act == ACT_HL2MP_JUMP) then

			act = ACT_HL2MP_JUMP_SLAM

		end

		seq = pl:SelectWeightedSequence( act )

	end

	// Don't keep switching sequences if we're already playing the one we want.
	if (pl:GetSequence() == seq) then return true end

	// Set and reset the sequence
	pl:SetPlaybackRate( 1.0 )
	pl:ResetSequence( seq )
	pl:SetCycle( 0 )

	return true

end

hook.Add( "SetPlayerAnimation", "MudkipPlayerAnimation", MudkipPlayerAnimation )

function SetCustomSpeeds(ply)
	timer.Simple(0.1, function()
		GAMEMODE:SetPlayerSpeed( ply, 190, 327.5 )
		ply:SetCrouchedWalkSpeed( 63.33 / 190 )
		ply:SetDuckSpeed(0.4)
		ply:SetUnDuckSpeed(0.2)
	end)
end

function GM:PlayerInitialSpawn( ply )
	// We will figure out something cool to put here
	// Maybe top scoring player, server uptime, last kill streak...
	ply:PrintMessage( HUD_PRINTTALK, "Visit the forum at http://forum.1chan.us")
	ply:PrintMessage( HUD_PRINTTALK, "Report issues at http://tinyurl.com/mudkipbugs")

	ply:SetTeam( TEAM_SPECTATOR )
	ply:ConCommand( "team_menu" )
	
	SetCustomSpeeds(ply)
end

function MudkipPlayerSpawn( ply )
	SetCustomSpeeds(ply)
	ply:StopSound("Mudkip_Weapon_Sax");
end

hook.Add("PlayerSpawn", "MudkipPlayerSpawn", MudkipPlayerSpawn)

function GM:PlayerLoadout( ply )
	ply:StripWeapons()

	if ply:Team() == TEAM_TRAINER then
		ply:PrintMessage(HUD_PRINTTALK, "You are a Trainer this round.  Kill all of those cute blue bastards!")

		// Weapons
		ply:Give( "swep_crowbar" )
		ply:Give( "swep_pistol" )
		ply:Give( "swep_smg1" )
		ply:Give( "swep_frag" )
		ply:Give( "swep_crossbow" )
		ply:Give( "swep_shotgun" )
		ply:Give( "swep_357" );
		ply:Give( "swep_rpg" )
		ply:Give( "swep_ar2" )
		
		// Primary ammo
		ply:GiveAmmo(25, "Pistol", true)
		ply:GiveAmmo(12, "357", true)
		ply:GiveAmmo(75, "SMG1", true)
		ply:GiveAmmo(12, "Buckshot", true)
		ply:GiveAmmo(7, "XBowBolt", true)
		ply:GiveAmmo(4, "rpg_round", true)
		ply:GiveAmmo(4, "grenade", true)
		
		// Secondary ammo
		ply:GiveAmmo(5, "SMG1_Grenade", true)
		ply:GiveAmmo(3, "AR2AltFire", true)

		// Extra
		ply:Give( "weapon_physcannon" )

	elseif ply:Team() == TEAM_MUDKIP then
		ply:PrintMessage(HUD_PRINTTALK, "You are a Mudkip this round.  Spread your lovable blue cuteness everywhere!")

		// Weapons
		ply:Give( "swep_mudkip" )

	end
end

function GM:OnDamagedByExplosion( ply, dmginfo )
	ply:SetDSP(0, false) // Disable ear ringing
end

function OverrideDeathSound()
	return true
end
hook.Add("PlayerDeathSound", "OverrideDeathSound", OverrideDeathSound)

local deathSoundTable = {"player/mudkip_aaaaaaaaa.wav", "player/mudkip_bawww.wav", "player/mudkip_bobby.wav", "player/mudkip_deanscream.wav", "player/mudkip_hood.wav", "player/mudkip_hwg.wav", "player/mudkip_mariodeath.wav", "player/mudkip_thepriceiswrong.wav", "player/mudkip_wilhelm.wav", "player/mudkip_yelp.wav"}
sound.Add(
{
    name = "Mudkip_Player_Death",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 85,
    sound = deathSoundTable
})

local deathSounds = Sound( "Mudkip_Player_Death" )
sound.Add(
{
    name = "Mudkip_Weapon_Sax",
    channel = CHAN_STATIC,
    volume = 1.0,
    soundlevel = 85,
    sound = "weapons/physcannon/mudkip_sax.wav"
})

function MudkipPlayerDeath( ply_victim, ent_inflictor, ply_killer )
	ply_victim:StopSound("Mudkip_Weapon_Sax");
	ply_victim:EmitSound(deathSounds);
end

hook.Add( "PlayerDeath", "MudkipPlayerDeath", MudkipPlayerDeath )

function GM:PlayerSwitchWeapon(ply, oldWeapon, newWeapon)
	if (IsValid(ply) && IsValid(newWeapon) && newWeapon:GetClass() == "weapon_physcannon") then
		ply:EmitSound("Mudkip_Weapon_Sax");
	else
		ply:StopSound("Mudkip_Weapon_Sax");
	end
end

function team_trainer( ply )
	ply:SetTeam( TEAM_TRAINER )
	ply:Spawn()
end

function team_mudkip( ply )
	ply:SetTeam( TEAM_MUDKIP )
	ply:Spawn()
end

concommand.Add( "team_trainer", team_trainer )
concommand.Add( "team_mudkip", team_mudkip )
